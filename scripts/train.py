"""
Skript to train diffusion models on image-mask pairs.

Parts of the code taken from:
https://github.com/openai/improved-diffusion/blob/main/scripts/image_train.py
"""


import argparse
import json
from pathlib import Path

from semantic_diffusion import dist_util, logger
from semantic_diffusion.datasets import get_training_loader
from semantic_diffusion.metrics import f1_score
from semantic_diffusion.resample import create_named_schedule_sampler
from semantic_diffusion.script_util import (
    add_dict_to_argparser,
    args_to_dict,
    create_model_and_diffusion,
    model_and_diffusion_defaults,
)
from semantic_diffusion.train_util import TrainLoop


def main():
    args = create_argparser().parse_args()
    config = vars(args)
    logger.configure(
        args.log_dir, format_strs=["wandb"] if args.wandb_logging else [], config=config
    )
    json.dump(config, open(Path(logger.get_dir()) / "config.json", "w"))

    logger.log("creating model and diffusion...")
    model, diffusion = create_model_and_diffusion(
        **args_to_dict(args, model_and_diffusion_defaults().keys())
    )
    model.to(dist_util.dev()).train()
    diffusion.to(dist_util.dev()).train()
    schedule_sampler = create_named_schedule_sampler(args.schedule_sampler, diffusion)

    logger.log("creating data loader...")
    train_data = get_training_loader(
        data_dir=args.data_dir,
        mode="training",
        diffusion_type=args.diffusion_type,
        use_pretrained_model=args.use_pretrained_model,
        batch_size=args.batch_size,
        shuffle=True,
        resolution=args.resolution,
        augmentation=args.augmentation,
        num_workers=args.num_workers,
    )
    logger.log(f"Found {len(train_data.dataset)} training samples")
    if args.validation_interval:
        validation_data = get_training_loader(
            data_dir=args.data_dir,
            mode="validation",
            diffusion_type=args.diffusion_type,
            use_pretrained_model=args.use_pretrained_model,
            batch_size=args.batch_size,
            shuffle=False,
            resolution=args.resolution,
            num_workers=args.num_workers,
        )
        logger.log(f"Found {len(validation_data.dataset)} validation samples")
    else:
        validation_data = None

    if args.diffusion_type == "gaussian":
        validation_metric_fn = lambda groundtruths, predictions: f1_score(
            groundtruths, predictions, 0.0
        )
    elif args.diffusion_type == "bernoulli":
        validation_metric_fn = lambda groundtruths, predictions: f1_score(
            groundtruths, predictions, 0.5
        )

    TrainLoop(
        model=model,
        diffusion=diffusion,
        train_data=train_data,
        validation_data=validation_data,
        batch_size=args.batch_size,
        train_steps=args.train_steps,
        lr=args.lr,
        ema_rate=args.ema_rate,
        validation_metric_fn=validation_metric_fn,
        validation_num_samples=args.validation_num_samples,
        log_interval=args.log_interval,
        validation_interval=args.validation_interval,
        save_interval=args.save_interval,
        resume_checkpoint=args.resume_checkpoint,
        schedule_sampler=schedule_sampler,
        weight_decay=args.weight_decay,
    ).run_loop()


def create_argparser():
    args = dict(
        data_dir="",
        log_dir="./logs",
        schedule_sampler="uniform",
        lr=1e-4,
        weight_decay=0.0,
        batch_size=1,
        augmentation=False,
        train_steps=-1,  # -1 train forever
        ema_rate="0.9999",  # comma-separated list of EMA values
        validation_num_samples=1,
        log_interval=100,
        validation_interval=0,
        save_interval=10000,
        resume_checkpoint="",
        wandb_logging=False,
        num_workers=0,
    )
    defaults = model_and_diffusion_defaults()
    defaults.update(args)
    parser = argparse.ArgumentParser()
    add_dict_to_argparser(parser, defaults)
    return parser


if __name__ == "__main__":
    main()
