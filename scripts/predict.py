"""
Skript to predict new segmentation mask for input images.

Parts of the code taken from:
https://github.com/openai/improved-diffusion/blob/main/scripts/image_sample.py
"""

import argparse
import json
import os
from pathlib import Path

import numpy as np
from semantic_diffusion import dist_util
from semantic_diffusion.datasets import get_test_dataloader
from semantic_diffusion.image_util import get_mask
from semantic_diffusion.predict_util import predict
from semantic_diffusion.script_util import (
    add_dict_to_argparser,
    create_model_and_diffusion,
    model_and_diffusion_defaults,
)
from tqdm import tqdm


def main():
    args = create_argparser().parse_args()

    config = json.load(open(Path(args.model_path).parent / "config.json"))
    print("creating model and diffusion...")
    model, diffusion = create_model_and_diffusion(
        **{k: config[k] for k in model_and_diffusion_defaults().keys()}
    )
    model.load_state_dict(
        dist_util.load_state_dict(args.model_path, map_location="cpu")
    )
    model.to(dist_util.dev()).eval()
    diffusion.to(dist_util.dev()).eval()

    print(f"Predicting using {dist_util.dev()}")
    input_images = get_test_dataloader(
        args.test_dir, config["use_pretrained_model"], args.batch_size, resolution=config["resolution"]
    )

    os.makedirs(Path(args.output_dir) / "raw", exist_ok=True)
    os.makedirs(Path(args.output_dir) / "masks", exist_ok=True)

    for images, names in tqdm(input_images):
        predictions = predict(
            model,
            diffusion,
            images.to(dist_util.dev()),
            args.num_mask_samples,
        ).cpu()

        for i, name in enumerate(names):
            np_mask = predictions[i].numpy()
            mask = get_mask(np_mask, resolution=400, threshold=args.threshold)
            np.save(Path(args.output_dir) / "raw" / str(name), np_mask)
            mask.save(Path(args.output_dir) / "masks" / str(name))

    print("Prediction complete")


def create_argparser():
    args = dict(
        test_dir="",
        output_dir="",
        model_path="",
        num_mask_samples=10,
        batch_size=1,
        threshold=0.5,
    )
    parser = argparse.ArgumentParser()
    add_dict_to_argparser(parser, args)
    return parser


if __name__ == "__main__":
    main()
