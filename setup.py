from setuptools import setup

setup(
    name="semantic-diffusion",
    author="Andreas Bergmeister",
    py_modules=["semantic_diffusion"],
    install_requires=[
        "scikit-learn",
        "scikit-image",
        "numpy",
        "torch",
        "imgaug",
        "albumentations",
        "tqdm",
        "pillow",
        "opencv-python",
        "python-dotenv",
        "wandb",
        "segmentation-models-pytorch",
    ],
)
