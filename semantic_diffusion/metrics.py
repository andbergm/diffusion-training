import sklearn


def f1_score(groundtruths, predictions , threshold):
    # convert to binary
    groundtruths[groundtruths <= threshold] = 0
    groundtruths[groundtruths > threshold] = 1

    predictions[predictions <= threshold] = 0
    predictions[predictions > threshold] = 1

    return sklearn.metrics.f1_score(
        groundtruths.flatten().cpu().numpy(),
        predictions.flatten().cpu().numpy()
    )
