import torch as th

@th.no_grad()
def predict(model, diffusion, images, num_samples):
    """
    Samples num_samples many mask predictions from the model.
    Outputs the average of all predicted samples.
    """
    mode = model.training
    model.eval()
    diffusion.eval()

    with th.no_grad():
        samples = []
        for _ in range(num_samples):
            sample = diffusion.p_sample_loop(
                model,
                images,
                progress=False,
            )
            samples.append(sample)

        sample_mean = th.mean(th.stack(samples), 0)

    #restore training mode
    model.train(mode)
    diffusion.train(mode)

    return sample_mean
