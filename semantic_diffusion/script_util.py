"""
Parts of the code taken from:
https://github.com/openai/improved-diffusion/blob/main/improved_diffusion/script_util.py
"""

import argparse


from .diffusion import BernoulliDiffusion, GaussianDiffusion, get_named_beta_schedule
from .unet import UNetModel
from .pretrained_model import PretrainedModelWrapper


def model_and_diffusion_defaults():
    """
    Defaults for image segemntation training.
    """
    return dict(
        resolution=256,
        use_pretrained_model=False,
        num_channels=128,
        num_res_blocks=2,
        num_heads=4,
        num_heads_upsample=-1,
        attention_resolutions="16,8",
        dropout=0.1,
        diffusion_type="gaussian",
        diffusion_steps=1000,
        noise_schedule="linear",
        loss_type="vlb",
        hybrid_loss_lambda=0.001,
        use_checkpoint=False,
        use_scale_shift_norm=True,
    )


def create_model_and_diffusion(
    resolution,
    use_pretrained_model,
    num_channels,
    num_res_blocks,
    num_heads,
    num_heads_upsample,
    attention_resolutions,
    dropout,
    diffusion_type,
    diffusion_steps,
    noise_schedule,
    loss_type,
    hybrid_loss_lambda,
    use_checkpoint,
    use_scale_shift_norm,
):
    if not use_pretrained_model:
        model = create_model(
            resolution,
            num_channels,
            num_res_blocks,
            use_checkpoint=use_checkpoint,
            attention_resolutions=attention_resolutions,
            num_heads=num_heads,
            num_heads_upsample=num_heads_upsample,
            use_scale_shift_norm=use_scale_shift_norm,
            dropout=dropout,
        )
    else:
        model = PretrainedModelWrapper(diffusion_steps=diffusion_steps)

    betas = get_named_beta_schedule(noise_schedule, diffusion_steps)

    if diffusion_type == "gaussian":
        diffusion = GaussianDiffusion(betas, loss_type, hybrid_loss_lambda)

    elif diffusion_type == "bernoulli":
        diffusion = BernoulliDiffusion(betas, loss_type, hybrid_loss_lambda)
    else:
        raise NotImplementedError(f"unknown diffusion type: {diffusion_type}")

    return model, diffusion


def create_model(
    resolution,
    num_channels,
    num_res_blocks,
    use_checkpoint,
    attention_resolutions,
    num_heads,
    num_heads_upsample,
    use_scale_shift_norm,
    dropout,
):
    if resolution == 512:
        channel_mult = (0.5, 1, 1, 2, 2, 4, 4)
    elif resolution == 256:
        channel_mult = (1, 1, 2, 2, 4, 4)
    elif resolution == 128:
        channel_mult = (1, 1, 2, 3, 4)
    elif resolution == 64:
        channel_mult = (1, 2, 3, 4)
    else:
        raise ValueError(f"unsupported image size: {resolution}")

    attention_ds = []
    for res in attention_resolutions.split(","):
        attention_ds.append(resolution // int(res))

    return UNetModel(
        in_channels=4,  # 3 RGB + 1 mask
        model_channels=num_channels,
        out_channels=1,
        num_res_blocks=num_res_blocks,
        attention_resolutions=tuple(attention_ds),
        dropout=dropout,
        channel_mult=channel_mult,
        use_checkpoint=use_checkpoint,
        num_heads=num_heads,
        num_heads_upsample=num_heads_upsample,
        use_scale_shift_norm=use_scale_shift_norm,
    )


def add_dict_to_argparser(parser, default_dict):
    for k, v in default_dict.items():
        v_type = type(v)
        if v is None:
            v_type = str
        elif isinstance(v, bool):
            v_type = str2bool
        parser.add_argument(f"--{k}", default=v, type=v_type)


def args_to_dict(args, keys):
    return {k: getattr(args, k) for k in keys}


def str2bool(v):
    """
    https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse
    """
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("boolean value expected")
