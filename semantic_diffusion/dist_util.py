import io

import torch as th


def dev():
    """
    Get the device to use.
    """
    if th.cuda.is_available():
        return th.device("cuda:0")
    return th.device("cpu")


def load_state_dict(path, **kwargs):
    with open(path, "rb") as f:
        data = f.read()
    return th.load(io.BytesIO(data), **kwargs)
