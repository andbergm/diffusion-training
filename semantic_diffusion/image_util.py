import numpy as np
from PIL import Image


def get_image(np_image, resolution=None):
    np_image = denormalize_image(np_image).clip(0, 255)
    np_image = np.transpose(np_image, [1, 2, 0])
    image = Image.fromarray(np.uint8(np_image))

    if resolution is not None:
        image = image.resize((resolution, resolution))

    return image


def get_mask(np_mask, resolution=None, threshold=0.5):
    np_mask[np_mask <= threshold] = 0
    np_mask[np_mask > threshold] = 1

    np_mask = (np_mask * 255)
    mask = Image.fromarray(np.uint8(np_mask), mode="L")

    if resolution is not None:
        mask = mask.resize((resolution, resolution))

    return mask


def get_binary_image(np_image, threshold=127.5):
    np_image[np_image <= threshold] = 0
    np_image[np_image > threshold] = 1
    return np_image


def normalize_image(np_image):
    return np_image / 127.5 - 1


def denormalize_image(np_image):
    return (np_image + 1) * 127.5
