"""
Parts of the code taken from:
https://github.com/openai/improved-diffusion/blob/main/improved_diffusion/gaussian_diffusion.py

NOTE: abuse of notation:
in the following, x_logits, referes to the log probability of x, i.e. ln(Pr[x = 1]);
(we don't mean the mathematical logit function)
"""

import math

import numpy as np
import torch as th
import torch.nn.functional as F
from torch.nn import Module
from tqdm.auto import tqdm

from .nn import mean_flat


def get_named_beta_schedule(schedule_name, num_diffusion_timesteps):
    """
    Get a pre-defined beta schedule for the given name.

    The beta schedule library consists of beta schedules which remain similar
    in the limit of num_diffusion_timesteps.
    Beta schedules may be added, but should not be removed or changed once
    they are committed to maintain backwards compatibility.
    """
    if schedule_name == "linear":
        # Linear schedule from Ho et al, extended to work for any number of
        # diffusion steps.
        scale = 1000 / num_diffusion_timesteps
        beta_start = scale * 0.0001
        beta_end = scale * 0.02
        return th.linspace(
            beta_start, beta_end, num_diffusion_timesteps, dtype=th.float64
        )
    elif schedule_name == "cosine":
        return betas_for_alpha_bar(
            num_diffusion_timesteps,
            lambda t: th.tensor(
                math.cos((t + 0.008) / 1.008 * math.pi / 2) ** 2, dtype=th.float64
            ),
        )
    else:
        raise NotImplementedError(f"unknown beta schedule: {schedule_name}")


def betas_for_alpha_bar(num_diffusion_timesteps, alpha_bar, max_beta=0.999):
    """
    Create a beta schedule that discretizes the given alpha_t_bar function,
    which defines the cumulative product of (1-beta) over time from t = [0,1].

    :param num_diffusion_timesteps: the number of betas to produce.
    :param alpha_bar: a lambda that takes an argument t from 0 to 1 and
                      produces the cumulative product of (1-beta) up to that
                      part of the diffusion process.
    :param max_beta: the maximum beta to use; use values lower than 1 to
                     prevent singularities.
    """
    betas = []
    for i in range(num_diffusion_timesteps):
        t1 = i / num_diffusion_timesteps
        t2 = (i + 1) / num_diffusion_timesteps
        betas.append(min(1.0 - alpha_bar(t2) / alpha_bar(t1), max_beta))
    return th.tensor(betas, dtype=th.float64)


def normal_kl(mean1, logvar1, mean2, logvar2):
    """
    Compute the KL divergence between two gaussians.

    Shapes are automatically broadcasted, so batches can be compared to
    scalars, among other use cases.
    """
    tensor = None
    for obj in (mean1, logvar1, mean2, logvar2):
        if isinstance(obj, th.Tensor):
            tensor = obj
            break
    assert tensor is not None, "at least one argument must be a Tensor"

    # Force variances to be Tensors. Broadcasting helps convert scalars to
    # Tensors, but it does not work for th.exp().
    logvar1, logvar2 = [
        x if isinstance(x, th.Tensor) else th.tensor(x).to(tensor)
        for x in (logvar1, logvar2)
    ]

    return 0.5 * (
        -1.0
        + logvar2
        - logvar1
        + th.exp(logvar1 - logvar2)
        + ((mean1 - mean2) ** 2) * th.exp(-logvar2)
    )


def approx_standard_normal_cdf(x):
    """
    A fast approximation of the cumulative distribution function of the
    standard normal.
    """
    return 0.5 * (1.0 + th.tanh(np.sqrt(2.0 / np.pi) * (x + 0.044715 * th.pow(x, 3))))


def discretized_gaussian_log_likelihood(x, *, means, log_scales):
    """
    Compute the log-likelihood of a Gaussian distribution discretizing to a
    given image.

    :param x: the target images. It is assumed that this was uint8 values,
              rescaled to the range [-1, 1].
    :param means: the Gaussian mean Tensor.
    :param log_scales: the Gaussian log stddev Tensor.
    :return: a tensor like x of log probabilities (in nats).
    """
    assert x.shape == means.shape == log_scales.shape
    centered_x = x - means
    inv_stdv = th.exp(-log_scales)
    plus_in = inv_stdv * (centered_x + 1.0 / 255.0)
    cdf_plus = approx_standard_normal_cdf(plus_in)
    min_in = inv_stdv * (centered_x - 1.0 / 255.0)
    cdf_min = approx_standard_normal_cdf(min_in)
    log_cdf_plus = log(cdf_plus)
    log_one_minus_cdf_min = log(1.0 - cdf_min)
    cdf_delta = cdf_plus - cdf_min
    log_probs = th.where(
        x < -0.999,
        log_cdf_plus,
        th.where(x > 0.999, log_one_minus_cdf_min, log(cdf_delta)),
    )
    assert log_probs.shape == x.shape
    return log_probs


def bernoulli_kl(q_logits, p_logits):
    """
    Returns the kl-divergence KL(q || p) of two bernoulli distributions
    with parameters e^q_logits and e^p_logits
    """
    kl = th.exp(q_logits) * (q_logits - p_logits)
    +(1 - th.exp(q_logits)) * (
        log_one_minus_exp(q_logits) - log_one_minus_exp(p_logits)
    )
    # kl divergence is positive (this is only for numerical stability)
    kl = kl.clamp(0.0)

    # return per batch mean
    return mean_flat(kl)


def bernoulli(p):
    return th.bernoulli(p.clamp(0.0, 1.0))


def bernoulli_logits(p):
    """
    Sample from Bernoullid distribution with parameter e^p
    """
    return bernoulli(th.exp(p))


def log(p):
    return th.log(p.clamp(1e-20))


def log_add_exp(a, b):
    """
    return log(e^a + e^b)
    """
    return th.max(a, b) + th.log1p(th.exp(-th.abs(a - b)))


def log_one_minus_exp(a):
    """
    return log(1 - e^a)
    """
    return log(-th.expm1(a))


def log_sub_exp(a, b):
    """
    return ln(e^a - e^b)
    asserts a > b
    """
    return a + th.log1p(-th.exp(b - a))


class GaussianDiffusion(Module):
    """
    Utilities for training and sampling diffusion models.

    :param betas: a 1-D numpy array of betas for each diffusion timestep,
                  starting at T and going to 1.
    :param loss_type: a LossType determining the loss function to use.
    """

    def __init__(
        self,
        betas,
        loss_type,
        hybrid_loss_lambda=0.001,
    ):
        super().__init__()
        self.loss_type = loss_type
        self.hybrid_loss_lambda = hybrid_loss_lambda

        # Use float64 for accuracy.
        assert betas.dtype == th.float64
        alphas = 1.0 - betas
        alphas_cumprod = th.cumprod(alphas, dim=0)
        alphas_cumprod_prev = th.cat([th.tensor([1.0]), alphas_cumprod[:-1]])
        posterior_variance = (
            betas * (1.0 - alphas_cumprod_prev) / (1.0 - alphas_cumprod)
        )
        self.num_timesteps = int(betas.shape[0])

        # register buffers (32 bit)
        register_buffer = lambda name, tensor: self.register_buffer(
            name, tensor.to(th.float32)
        )

        register_buffer("alphas_cumprod", alphas_cumprod)
        register_buffer("alphas_cumprod_prev", alphas_cumprod_prev)
        register_buffer("alphas_cumprod_sqrt", th.sqrt(alphas_cumprod))
        register_buffer(
            "one_minus_alphas_cumprod_sqrt", th.sqrt(1.0 - self.alphas_cumprod)
        )
        register_buffer("recip_alphas_cumprod_sqrt", th.sqrt(1.0 / alphas_cumprod))
        register_buffer(
            "recipm1_alphas_cumprod_sqrt", th.sqrt(1.0 / alphas_cumprod - 1)
        )
        register_buffer("posterior_log_variance", log(posterior_variance))
        register_buffer(
            "posterior_mean_coef1",
            betas * np.sqrt(self.alphas_cumprod_prev) / (1.0 - self.alphas_cumprod),
        )
        register_buffer(
            "posterior_mean_coef2",
            (1.0 - self.alphas_cumprod_prev)
            * np.sqrt(alphas)
            / (1.0 - self.alphas_cumprod),
        )

    def q_sample(self, x_0, t, eps=None):
        """
        Diffuse the data for a given number of diffusion steps.

        In other words, sample from q(x_t | x_0).

        :param x_0: the initial data batch.
        :param t: the number of diffusion steps (minus 1). Here, 0 means one step.
        :param eps: if specified, the split-out normal noise.
        :return: A noisy version of x_0.
        """
        if eps is None:
            eps = th.randn_like(x_0)
        return (
            _extract_into_tensor(self.alphas_cumprod_sqrt, t, x_0.shape) * x_0
            + _extract_into_tensor(self.one_minus_alphas_cumprod_sqrt, t, x_0.shape)
            * eps
        )

    def q_posterior_mean_variance(self, x_0, x_t, t):
        """
        Compute the mean and variance of the diffusion posterior:

            q(x_{t-1} | x_t, x_0)

        """
        assert x_0.shape == x_t.shape
        expand = lambda x: _extract_into_tensor(x, t, x_t.shape)

        posterior_mean = (
            expand(self.posterior_mean_coef1) * x_0
            + expand(self.posterior_mean_coef2) * x_t
        )
        posterior_log_variance = expand(self.posterior_log_variance)

        return posterior_mean, posterior_log_variance

    def _predict_x0_from_eps(self, x_t, t, eps):
        assert x_t.shape == eps.shape
        return (
            _extract_into_tensor(self.recip_alphas_cumprod_sqrt, t, x_t.shape) * x_t
            - _extract_into_tensor(self.recipm1_alphas_cumprod_sqrt, t, x_t.shape) * eps
        )

    def p_mean_variance(self, model, img, x_t, t, clip_denoised=True):
        """
        Use the model to get the parameters for p(x_{t-1} | x_t)

        :param model: the model, which takes a signal and a batch of timesteps
                      as input.
        :param img: the input images, (N, C, H, W).
        :param x: the [N x C x ...] tensor at time t.
        :param t: a 1-D Tensor of timesteps.
        :param clip_denoised: if True, clip the denoised signal into [-1, 1].
        :return: parameters (mean and log-variance) for p(x_{t-1} | x_t)
        """

        eps_pred = model(th.cat([img, x_t.unsqueeze(1)], dim=1), t).squeeze(1)

        x_0_pred = self._predict_x0_from_eps(x_t=x_t, t=t, eps=eps_pred)
        if clip_denoised:
            x_0_pred = x_0_pred.clamp(-1, 1)

        return self.q_posterior_mean_variance(x_0=x_0_pred, x_t=x_t, t=t)

    @th.no_grad()
    def p_sample(self, model, img, x, t, clip_denoised=True):
        """
        Use the model to sample from p(x_{t-1} | x_t) = q(x_{t-1} | x_t, x_0^)

        :param model: the model, which takes a signal and a batch of timesteps as input.
        :param img: the input images, (N, C, H, W).
        :param x_t: the mask (N, H, W) at time t.
        :param t: a 1-D Tensor of timesteps.
        :param clip_denoised: if True, clip the x_0 prediction to [-1, 1].
        :return: sample x_{t-1}, (N, H, W)
        """
        mean, log_variance = self.p_mean_variance(
            model,
            img,
            x,
            t,
            clip_denoised=clip_denoised,
        )
        eps = th.randn_like(x)
        nonzero_mask = (
            (t != 0).float().view(-1, *([1] * (len(x.shape) - 1)))
        )  # no noise when t == 0
        return mean + nonzero_mask * th.exp(0.5 * log_variance) * eps

    @th.no_grad()
    def p_sample_loop(
        self,
        model,
        img,
        clip_denoised=True,
        progress=False,
    ):
        """
        Generate segmentation mask for the image from the model.

        :param model: the model module.
        :param img: the input images, (N, C, H, W).
        :param clip_denoised: if True, clip x_0 predictions to [-1, 1].
        :param progress: if True, show a tqdm progress bar.
        :return: a non-differentiable batch of samples, (N, H, W).
        """
        final = None
        for sample in self.p_sample_loop_progressive(
            model,
            img=img,
            clip_denoised=clip_denoised,
            progress=progress,
        ):
            final = sample
        return final

    @th.no_grad()
    def p_sample_loop_progressive(
        self,
        model,
        img,
        clip_denoised=True,
        progress=False,
    ):
        """
        Generate segmentation mask samples from the model and yield intermediate samples from
        each timestep of diffusion.

        Arguments are the same as p_sample_loop().
        Returns a generator over samples from q_sample().
        """
        device = img.device
        B, C, H, W = img.shape

        # sample x_T from normal distribution
        x_t_sample = th.randn(B, H, W, device=device)

        indices = list(range(self.num_timesteps))[::-1]

        if progress:
            indices = tqdm(indices)

        for i in indices:
            t = th.tensor([i] * img.shape[0], device=device)
            x_t_sample = self.p_sample(
                model, img, x_t_sample, t, clip_denoised=clip_denoised
            )
            yield x_t_sample

    def training_losses(self, model, img, x_0, t):
        """
        Compute training losses for a single timestep.

        :param model: the model to evaluate loss on.
        :param x_0: the [N x C x ...] tensor of inputs.
        :param t: a batch of timestep indices.
        :param model_kwargs: if not None, a dict of extra keyword arguments to
            pass to the model. This can be used for conditioning.
        :return: a dict with various loss terms containing a tensor of shape [N].
        """

        eps = th.randn_like(x_0)
        x_t = self.q_sample(x_0, t, eps=eps)

        eps_pred = model(
            th.cat([img, x_t.unsqueeze(1)], dim=1),
            t,
        ).squeeze(1)

        terms = {}
        mse = 0
        vlb = 0
        if self.loss_type == "mse" or self.loss_type == "hybrid":
            mse = mean_flat((eps - eps_pred) ** 2)
            terms["mse"] = mse
            loss = mse
        elif self.loss_type == "vlb" or self.loss_type == "hybrid":
            vlb = self.vlb_loss(
                model=model,
                img=img,
                x_0=x_0,
                x_t=x_t,
                t=t,
            )
            terms["vlb"] = vlb
            loss = vlb

        if self.loss_type == "hybrid":
            loss = vlb + self.hybrid_loss_lambda * mse

        terms["loss"] = loss
        return terms

    def vlb_loss(
        self,
        model,
        img,
        x_0,
        x_t,
        t,
    ):
        true_mean, true_log_variance = self.q_posterior_mean_variance(
            x_0=x_0, x_t=x_t, t=t
        )
        mean, log_variance = self.p_mean_variance(
            model,
            img,
            x_t,
            t,
            clip_denoised=False,
        )
        kl = normal_kl(true_mean, true_log_variance, mean, log_variance)
        kl = mean_flat(kl) / np.log(2.0)

        decoder_nll = -discretized_gaussian_log_likelihood(
            x_0, means=mean, log_scales=0.5 * log_variance
        )
        assert decoder_nll.shape == x_0.shape
        decoder_nll = mean_flat(decoder_nll) / np.log(2.0)

        # At the first timestep return the decoder NLL,
        # otherwise return KL(q(x_{t-1}|x_t,x_0) || p(x_{t-1}|x_t))
        loss = th.where((t == 0), decoder_nll, kl)
        # in contrast to the nll loss, we have to scale the vlb loss with the number of timesteps
        loss *= self.num_timesteps
        return loss


class BernoulliDiffusion(Module):
    """
    Utilities for training and sampling diffusion models.

    TODO: add docstring
    :param betas: a 1-D numpy array of betas for each diffusion timestep,
                  starting at T and going to 1.
    :param loss_type: a LossType determining the loss function to use.
    """

    def __init__(
        self,
        betas,
        loss_type,
        hybrid_loss_lambda=0.001,
    ):
        super().__init__()
        self.loss_type = loss_type
        self.hybrid_loss_lambda = hybrid_loss_lambda

        # Use float64 for accuracy.
        assert betas.dtype == th.float64
        betas_log = log(betas)
        alphas = 1.0 - betas
        alphas_cumprod = th.cumprod(alphas, dim=0)
        alphas_cumprod_prev = th.cat([th.tensor([1.0]), alphas_cumprod[:-1]])
        alphas_log = log(alphas)
        alphas_cumprod_log = th.cumsum(alphas_log, dim=0)
        alphas_cumprod_prev_log = th.cat([th.tensor([0.0]), alphas_cumprod_log[:-1]])
        self.num_timesteps = int(betas.shape[0])

        # register buffers (32 bit)
        register_buffer = lambda name, tensor: self.register_buffer(
            name, tensor.to(th.float32)
        )

        register_buffer("betas_half_log", betas_log - np.log(2.0))
        register_buffer("alphas_log", alphas_log)
        register_buffer("alphas_cumprod_log", alphas_cumprod_log)
        register_buffer("alphas_cumprod_prev_log", alphas_cumprod_prev_log)
        register_buffer(
            "one_minus_alphas_cumprod_half_log",
            log(1.0 - alphas_cumprod) - np.log(2.0),
        )
        register_buffer(
            "one_minus_alphas_cumprod_prev_half_log",
            log(1.0 - alphas_cumprod_prev) - np.log(2.0),
        )

    def q_x_t_logits(self, x_0_logits, t):
        """
        Return log-probability of q(x_t = 1 | x_0)
        """
        expand = lambda x: _extract_into_tensor(x, t, x_0_logits.shape)

        return log_add_exp(
            expand(self.alphas_cumprod_log) + x_0_logits,
            expand(self.one_minus_alphas_cumprod_half_log),
        )

    def q_sample(self, x_0_logits, t):
        """
        Sample from q(x_t | x_0).
        """
        x_t_logits = self.q_x_t_logits(x_0_logits, t)
        return bernoulli_logits(x_t_logits)

    def q_posterior_logits(self, x_t_logits, x_0_logits, t):
        """
        Return log-probability of q(x_{t-1} = 1 | x_t, x_0)
        """
        # log q(x_{t-1} = 1 | x_t, x_0)
        # = log ((term_1 * term2) / (term3 + term4))
        # = term1_log + term2_log - log_add_exp(term3_log, term4_log)

        expand = lambda x: _extract_into_tensor(x, t, x_0_logits.shape)

        term1_log = log_add_exp(
            expand(self.alphas_log) + x_t_logits, expand(self.betas_half_log)
        )

        term2_log = log_add_exp(
            expand(self.alphas_cumprod_prev_log) + x_0_logits,
            expand(self.one_minus_alphas_cumprod_prev_half_log),
        )

        term3_log = log_add_exp(
            expand(self.alphas_cumprod_log) + x_0_logits + x_t_logits,
            expand(self.one_minus_alphas_cumprod_half_log) + x_t_logits,
        )

        log_1_minus_x_t = log_one_minus_exp(x_t_logits)
        term4_log = log_sub_exp(
            log_1_minus_x_t,
            log_add_exp(
                expand(self.alphas_cumprod_log) + x_0_logits + log_1_minus_x_t,
                expand(self.one_minus_alphas_cumprod_half_log) + log_1_minus_x_t,
            ),
        )

        return term1_log + term2_log - log_add_exp(term3_log, term4_log)

    def p_x_0_logits(self, model, img, x_t, t):
        """
        Returns the predicted log-probability of p(x_0_hat = 1 | x_t, t) using the model

        :param model: the model, which takes a signal and a batch of timesteps as input.
        :param img: the input images, (N, C, H, W).
        :param x_t: the mask (N, H, W) at time t.
        :param t: a 1-D Tensor of timesteps.
        :return: probability vector for x_0, (N, H, W)
        """
        return F.logsigmoid(
            model(
                th.cat([img, x_t.unsqueeze(1)], dim=1),
                t,
            ).squeeze(1)
        )

    @th.no_grad()
    def p_sample(self, model, img, x_t, t):
        """
        Use the model to sample from p(x_{t-1} | x_t) = q(x_{t-1} | x_t, x_0^)

        :param model: the model, which takes a signal and a batch of timesteps as input.
        :param img: the input images, (N, C, H, W).
        :param x_t: the mask (N, H, W) at time t.
        :param t: a 1-D Tensor of timesteps.
        :return: sample x_{t-1}, (N, H, W)
        """
        x_0_hat_logits = self.p_x_0_logits(model, img, x_t, t)
        x_0_sample = bernoulli_logits(x_0_hat_logits)
        x_t_min1_logit = self.q_posterior_logits(
            x_t_logits=log(x_t), x_0_logits=x_0_hat_logits, t=t
        )
        x_t_min1_sample = bernoulli_logits(x_t_min1_logit)

        # return x_0_sample for t == 0, otherwise sample from posterior
        return th.where(
            t.view(-1, *([1] * (len(x_0_hat_logits.shape) - 1))) == 0,
            x_0_sample,
            x_t_min1_sample,
        )

    @th.no_grad()
    def p_sample_loop(self, model, img, progress=False):
        """
        Generate segmentation mask for the image.

        :param model: the model module.
        :param img: the input images, (N, C, H, W).
        :param progress: if True, show a tqdm progress bar.
        :return: a non-differentiable batch of samples, (N, H, W).
        """
        final = None
        for sample in self.p_sample_loop_progressive(
            model,
            img=img,
            progress=progress,
        ):
            final = sample
        return final

    @th.no_grad()
    def p_sample_loop_progressive(self, model, img, progress=False):
        """
        Generate segmentation mask samples from the model and yield intermediate samples from
        each timestep of diffusion.

        Arguments are the same as p_sample_loop().
        Returns a generator over samples from q_sample().
        """
        device = img.device

        # sample x_T from uniform bernoulli distribution
        x_t_sample = bernoulli(th.zeros_like(img[:, 0]) + 0.5)

        indices = list(range(self.num_timesteps))[::-1]
        if progress:
            indices = tqdm(indices)

        for i in indices:
            t = th.tensor([i] * img.shape[0], device=device)
            x_t_sample = self.p_sample(
                model,
                img,
                x_t_sample,
                t,
            )
            yield x_t_sample

    def training_losses(self, model, img, x_0, t):
        """
        Compute training losses for a single timestep.

        :param model: the model to evaluate loss on.
        :param img: a batch of images, (N, 3, H, W)
        :param x_0: a batch of corresponding gorundtruth mask, (N, H, W).
        :param t: a batch of timestep indices.
        :return: loss average per batch element
        :return: a dict with various losses averaged per batch

        """
        x_t = self.q_sample(x_0_logits=log(x_0), t=t)
        x_0_hat_logits = self.p_x_0_logits(model, img, x_t, t)

        terms = {}
        nll = 0
        vlb = 0
        if self.loss_type == "nll" or self.loss_type == "hybrid":
            nll = self.nll(x_0, x_0_hat_logits)
            terms["nll"] = nll
            loss = nll
        elif self.loss_type == "vlb" or self.loss_type == "hybrid":
            vlb = self.vlb_loss(
                x_0=x_0, x_t_logits=log(x_t), x_0_hat_logits=x_0_hat_logits, t=t
            )
            terms["vlb"] = vlb
            loss = vlb

        if self.loss_type == "hybrid":
            loss = vlb + self.hybrid_loss_lambda * nll

        terms["loss"] = loss
        return terms

    def vlb_loss(self, x_0, x_t_logits, x_0_hat_logits, t):
        q_logits = self.q_posterior_logits(
            x_t_logits=x_t_logits, x_0_logits=log(x_0), t=t
        )
        p_logits = self.q_posterior_logits(
            x_t_logits=x_t_logits, x_0_logits=x_0_hat_logits, t=t
        )
        kl_div = bernoulli_kl(q_logits, p_logits)
        nll = self.nll(x_0, x_0_hat_logits)
        # return negative log-likelihood for t=0, otherwise kl-divergence
        loss = th.where(t == 0, nll, kl_div)
        # in contrast to the nll loss, we have to scale the vlb loss with the number of timesteps
        loss *= self.num_timesteps
        return loss

    def nll(self, x_0, x_0_hat_logits):
        ll = x_0 * x_0_hat_logits + (1 - x_0) * log_one_minus_exp(x_0_hat_logits)
        # return per batch mean
        return mean_flat(-ll)


def _extract_into_tensor(arr, t, broadcast_shape):
    """
    Extract values from a 1-D numpy array for a batch of indices.

    :param arr: the 1-D numpy array.
    :param t: a tensor of indices into the array to extract.
    :param broadcast_shape: a larger shape of K dimensions with the batch
                            dimension equal to the length of t.
    :return: a tensor of shape [batch_size, 1, ...] where the shape has K dims.
    """
    return arr[t].view(-1, *([1] * (len(broadcast_shape) - 1))).expand(broadcast_shape)
