"""
Parts of the code taken from:
https://github.com/openai/improved-diffusion/blob/main/improved_diffusion/train_util.py
"""

import copy
import os

import numpy as np
import torch as th
from torch.optim import AdamW

from . import dist_util, logger
from .nn import update_ema
from .predict_util import predict
from .resample import LossAwareSampler, UniformSampler


class TrainLoop:
    def __init__(
        self,
        *,
        model,
        diffusion,
        train_data,
        validation_data,
        batch_size,
        train_steps,
        lr,
        ema_rate,
        validation_metric_fn,
        validation_num_samples,
        log_interval,
        validation_interval,
        save_interval,
        resume_checkpoint,
        schedule_sampler=None,
        weight_decay=0.0,
    ):
        self.model = model
        self.diffusion = diffusion
        self.train_data = train_data
        self.validation_data = validation_data
        self.batch_size = batch_size
        self.train_steps = train_steps
        self.lr = lr
        self.ema_rate = (
            [ema_rate]
            if isinstance(ema_rate, float)
            else [float(x) for x in ema_rate.split(",")]
        )
        self.validation_metric_fn = validation_metric_fn
        self.validation_num_samples = validation_num_samples
        self.log_interval = log_interval
        self.validation_interval = validation_interval
        self.save_interval = save_interval
        self.resume_checkpoint = resume_checkpoint

        self.schedule_sampler = schedule_sampler or UniformSampler(diffusion)
        self.weight_decay = weight_decay

        self.step = 0
        self.resume_step = 0

        self.model_params = list(self.model.parameters())
        self.master_params = self.model_params

        self._load_parameters()

        self.opt = AdamW(self.master_params, lr=self.lr, weight_decay=self.weight_decay)
        if self.resume_step:
            self._load_optimizer_state()
            # Model was resumed, either due to a restart or a checkpoint
            # being specified at the command line.
            self.ema_params = [
                self._load_ema_parameters(rate) for rate in self.ema_rate
            ]
        else:
            self.ema_params = [
                copy.deepcopy(self.master_params) for _ in range(len(self.ema_rate))
            ]

    def _load_parameters(self):
        resume_checkpoint = self.resume_checkpoint

        if resume_checkpoint:
            self.resume_step = parse_resume_step_from_filename(resume_checkpoint)
            logger.log(f"loading model from checkpoint: {resume_checkpoint}...")
            self.model.load_state_dict(
                dist_util.load_state_dict(
                    resume_checkpoint, map_location=dist_util.dev()
                )
            )

    def _load_ema_parameters(self, rate):
        ema_params = copy.deepcopy(self.master_params)

        main_checkpoint = self.resume_checkpoint
        ema_checkpoint = find_ema_checkpoint(main_checkpoint, self.resume_step, rate)
        if ema_checkpoint:
            logger.log(f"loading EMA from checkpoint: {ema_checkpoint}...")
            state_dict = dist_util.load_state_dict(
                ema_checkpoint, map_location=dist_util.dev()
            )
            ema_params = self._state_dict_to_master_params(state_dict)

        return ema_params

    def _load_optimizer_state(self):
        main_checkpoint = self.resume_checkpoint
        opt_checkpoint = os.path.join(
            os.path.dirname(main_checkpoint), f"opt{self.resume_step:06}.pt"
        )
        if os.path.exists(opt_checkpoint):
            logger.log(f"loading optimizer state from checkpoint: {opt_checkpoint}")
            state_dict = dist_util.load_state_dict(opt_checkpoint)
            self.opt.load_state_dict(state_dict)

    def run_loop(self):
        logger.log(f"Training model on {dist_util.dev()}")
        self.model.train()

        def train_data_generator():
            while True:
                yield from self.train_data

        while self.train_steps < 0 or self.step + self.resume_step < self.train_steps:
            img_batch, mask_batch = next(train_data_generator())
            # combine image and mask into one tensor
            # img_batch.shape: N x 3 x H x W
            # mask_batch.shape: N x H x W
            # batch.shape: N x 4 x H x W

            self.run_step(img_batch, mask_batch)
            self.step += 1
            if self.validation_interval and self.step % self.validation_interval == 0:
                self.run_validation()
            if self.log_interval and self.step % self.log_interval == 0:
                logger.dumpkvs()
            if self.save_interval and self.step % self.save_interval == 0:
                self.save()
        # Save the last checkpoint if it wasn't already saved.
        if self.save_interval and (self.step - 1) % self.save_interval != 0:
            self.save()

    def run_step(self, img_batch, mask_batch):
        self.forward_backward(img_batch, mask_batch)
        self.optimize()
        self.log_step()

    def forward_backward(self, img_batch, mask_batch):
        zero_grad(self.model_params)

        t, weights = self.schedule_sampler.sample(img_batch.shape[0], dist_util.dev())

        losses = self.diffusion.training_losses(
            self.model,
            img_batch.to(dist_util.dev()),
            mask_batch.to(dist_util.dev()),
            t,
        )

        if isinstance(self.schedule_sampler, LossAwareSampler):
            self.schedule_sampler.update_with_all_losses(t, losses["loss"].detach())

        loss = (losses["loss"] * weights).mean()
        log_loss_dict(self.diffusion, t, {k: v * weights for k, v in losses.items()})
        loss.backward()

    def run_validation(self):
        logger.log(f"Evaluating model at {self.step + self.resume_step} steps")
        predictions = []
        groundtruths = []
        for images, masks in self.validation_data:
            predictions.append(
                predict(
                    self.model,
                    self.diffusion,
                    images.to(dist_util.dev()),
                    self.validation_num_samples,
                )
            )
            groundtruths.append(masks)

        predictions = th.cat(predictions)
        groundtruths = th.cat(groundtruths)

        score = self.validation_metric_fn(groundtruths, predictions)
        logger.logkv("evaluation_score", score)
        return

    def optimize(self):
        self._log_grad_norm()
        self.opt.step()
        for rate, params in zip(self.ema_rate, self.ema_params):
            update_ema(params, self.master_params, rate=rate)

    def _log_grad_norm(self):
        sqsum = 0.0
        for p in self.master_params:
            if p.grad is not None:
                sqsum += (p.grad**2).sum().item()
        logger.logkv_mean("grad_norm", np.sqrt(sqsum))

    def log_step(self):
        logger.logkv("step", self.step + self.resume_step)
        logger.logkv("samples", (self.step + self.resume_step + 1) * self.batch_size)

    def save(self):
        def save_checkpoint(rate, params):
            state_dict = self._master_params_to_state_dict(params)
            logger.log(f"saving model {rate}...")
            if not rate:
                filename = f"model{(self.step+self.resume_step):06d}.pt"
            else:
                filename = f"ema_{rate}_{(self.step+self.resume_step):06d}.pt"
            with open(os.path.join(logger.get_dir(), filename), "wb") as f:
                th.save(state_dict, f)

        save_checkpoint(0, self.master_params)
        for rate, params in zip(self.ema_rate, self.ema_params):
            save_checkpoint(rate, params)

        with open(
            os.path.join(logger.get_dir(), f"opt{(self.step+self.resume_step):06d}.pt"),
            "wb",
        ) as f:
            th.save(self.opt.state_dict(), f)

    def _master_params_to_state_dict(self, master_params):
        state_dict = self.model.state_dict()
        for i, (name, _value) in enumerate(self.model.named_parameters()):
            assert name in state_dict
            state_dict[name] = master_params[i]
        return state_dict

    def _state_dict_to_master_params(self, state_dict):
        params = [state_dict[name] for name, _ in self.model.named_parameters()]
        return params


def parse_resume_step_from_filename(filename):
    """
    Parse filenames of the form path/to/modelNNNNNN.pt, where NNNNNN is the
    checkpoint's number of steps.
    """
    split = filename.split("model")
    if len(split) < 2:
        return 0
    split1 = split[-1].split(".")[0]
    try:
        return int(split1)
    except ValueError:
        return 0


def find_ema_checkpoint(main_checkpoint, step, rate):
    if main_checkpoint is None:
        return None
    filename = f"ema_{rate}_{(step):06d}.pt"
    path = os.path.join(os.path.dirname(main_checkpoint), filename)
    if os.path.exists(path):
        return path
    return None


def log_loss_dict(diffusion, ts, losses):
    for key, values in losses.items():
        logger.logkv_mean(key, values.mean().item())
        # Log the quantiles (four quartiles, in particular).
        for sub_t, sub_loss in zip(ts.cpu().numpy(), values.detach().cpu().numpy()):
            quartile = int(4 * sub_t / diffusion.num_timesteps)
            logger.logkv_mean(f"{key}_q{quartile}", sub_loss)


def zero_grad(model_params):
    for param in model_params:
        # Taken from https://pytorch.org/docs/stable/_modules/torch/optim/optimizer.html#Optimizer.add_param_group
        if param.grad is not None:
            param.grad.detach_()
            param.grad.zero_()
