import segmentation_models_pytorch as smp
import torch as th
from torch.nn import Module


class PretrainedModelWrapper(Module):
    def __init__(self, diffusion_steps):
        super().__init__()

        self.diffusion_steps = diffusion_steps
        self.model = smp.UnetPlusPlus(
            encoder_name="efficientnet-b5",
            encoder_weights="imagenet",
            in_channels=5,  # RGB + mask + timestep
            classes=1,  # mask
        )

    def forward(self, x, timesteps):
        """
        Concatenate x and timesteps and pass to super().forward()
        """
        emb = timesteps / self.diffusion_steps
        emb = (
            emb.unsqueeze(dim=-1)
            .unsqueeze(dim=-1)
            .expand(-1, *(x.shape[2:]))
            .unsqueeze(dim=1)
        )  # N -> N, 1, H, W

        return self.model(th.cat([x, emb], dim=1))
